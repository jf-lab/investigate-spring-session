package nl.ftoc.spring.problem.mongosession;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.userdetails.User;

import java.awt.*;
import java.time.LocalDateTime;

@Data
@Document(collection="cars")
public class Car {
    @Id
    private String id;
    @CreatedBy
    private User user;
    @CreatedDate
    private LocalDateTime createdDate;
    private String brand;
    private int doors = 4;
    private int wheels = 4;
    private Color color;
}
