package nl.ftoc.spring.problem.mongosession;

import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.session.data.mongo.config.annotation.web.reactive.EnableMongoWebSession;

@EnableWebFluxSecurity
@EnableMongoWebSession
@EnableMongoAuditing
public class SecurityConfiguration {
}
