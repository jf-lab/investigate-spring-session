package nl.ftoc.spring.problem.mongosession;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.WebSession;

import java.awt.*;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestSessionController {

    private final CarRepository repository;

    @GetMapping
    ResponseEntity<?> get(WebSession session) {
        log.info("My session: {}", session.getAttributes().entrySet());
        return ResponseEntity.ok("hello!");
    }

    @GetMapping("/create-car")
    ResponseEntity<?> createCar() {
        Car car = new Car();
        car.setBrand("Mercedes");
        car.setId(UUID.randomUUID().toString());
        car.setColor(Color.BLACK);
        Car savedCar = repository.save(car);
        return ResponseEntity.ok(savedCar);
    }

}
