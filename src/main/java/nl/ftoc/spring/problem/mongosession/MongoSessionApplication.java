package nl.ftoc.spring.problem.mongosession;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import java.util.Optional;

import static org.springframework.http.MediaType.IMAGE_GIF;
import static org.springframework.http.MediaType.TEXT_HTML;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Slf4j
@SpringBootApplication
public class MongoSessionApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongoSessionApplication.class, args);
    }
    @Bean
    public RouterFunction<ServerResponse> htmlRouter(
            @Value("classpath:/public/index.html") Resource indexHtml,
            @Value("classpath:/public/favicon.gif") Resource icon
    ) {
        var index = route(
                GET("/"),
                request -> ok()
                        .contentType(TEXT_HTML)
                        .syncBody(indexHtml)
        );

        var favicon = route(
                GET("/favicon.ico"),
                request -> ok()
                        .contentType(IMAGE_GIF)
                        .syncBody(icon)
        );

        return index.and(favicon);
    }


}
